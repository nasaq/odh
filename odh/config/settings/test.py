from .base import *
ALLOWED_HOSTS=["*"]
FORCE_SCRIPT_NAME = '/odh'
STATIC_URL = '/odh/static/'
LOCATION_FIELD_PATH = STATIC_URL + 'location_field'
LOCATION_FIELD = {
    'map.provider': 'google',
    'map.zoom': 13,

    'search.provider': 'google',
    'search.suffix': '',

    # Google
    'provider.google.api': '//maps.google.com/maps/api/js',
    'provider.google.api_key': GOOGLE_MAPS_V3_APIKEY,
    'provider.google.map_type': 'ROADMAP',

    # Mapbox
    'provider.mapbox.access_token': '',
    'provider.mapbox.max_zoom': 18,
    'provider.mapbox.id': 'mapbox.streets',

    # OpenStreetMap
    'provider.openstreetmap.max_zoom': 18,

    # misc
    'resources.root_path': LOCATION_FIELD_PATH,
    'resources.media': {
        'js': [
            LOCATION_FIELD_PATH + '/js/jquery.livequery.js',
            LOCATION_FIELD_PATH + '/js/form.js',
        ],
    },
}

LOGIN_URL = '/odh/accounts/login/'
LOGOUT_REDIRECT_URL = '/odh/accounts/login/'
LOGIN_REDIRECT_URL= '/odh/projects/home/'

LANGUAGE_CODE = 'ar'

LANGUAGES = (
            ('ar', gettext_noop('Arabic')),
             )