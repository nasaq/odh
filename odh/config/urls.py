from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy
from django.conf.urls.i18n import i18n_patterns

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

urlpatterns += i18n_patterns(
url(r'^$', RedirectView.as_view(url=reverse_lazy('home'))),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include("apps.accounts.urls")),
    url(r'^projects/', include("apps.projects.urls")),
    url(r'^forms/', include("forms_builder.forms.urls")),
)
