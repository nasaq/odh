from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    # customized user model (possibility of expansion)
    REQUIRED_FIELDS = []

    class Meta:
        abstract=False

    def __str__(self):
        return self.get_full_name()
