from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from apps.accounts.models import User


class Auth(TestCase):
    # login, logout test
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', email='test@test.pl', password='test')

    def test_registartion_url(self):
        response = self.client.post(reverse('registration'),
                                    {'email': 'test1@test.pl', 'username': 'test1', 'password': 'test1',
                                     'password2': 'test1'}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_login_url(self):
        response = self.client.post(reverse('login'), {'username': 'test', 'password': 'test'}, follow=True)
        last_url, status_code = response.redirect_chain[-1]
        self.assertEqual(last_url, reverse('home'))

    def test_logout_url(self):
        response = self.client.get(reverse('logout'), {}, follow=True)
        self.assertEqual(response.status_code, 200)
