from django import forms
from .models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import AuthenticationForm


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = _('Username')
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['placeholder'] = _('Password')
        self.fields['password'].widget.attrs['class'] = 'form-control'


class RegistrationAdminForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Username'), 'class': 'form-control'}))
    email = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Email'), 'class': 'form-control'}))
    password = forms.CharField(
        min_length=8,
        widget=forms.PasswordInput(attrs={'placeholder': _('Password'), 'class': 'form-control'}))
    password2 = forms.CharField(
        min_length=8,
        widget=forms.PasswordInput(attrs={'placeholder': _('Confirm password'), 'class': 'form-control'}))

    error_messages = {
        'password_mismatch': _("Passwords don't match.")
    }

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password2')
        widgets = {
            'password': forms.PasswordInput,
        }

    def clean_password2(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        if password and password2:
            if password != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_missmatch'
                )
        return password
