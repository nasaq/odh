from django.http import HttpResponseRedirect
from django.views.generic import CreateView
from .forms import RegistrationAdminForm
from django.core.urlresolvers import reverse_lazy


class RegistrationAdminView(CreateView):
    form_class = RegistrationAdminForm
    template_name = 'accounts/registration.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse_lazy('home'))

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password'])
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return HttpResponseRedirect(reverse_lazy('login'))
