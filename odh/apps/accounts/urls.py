from django.conf.urls import url
from django.contrib.auth.views import logout, login
from .views import RegistrationAdminView
from .forms import LoginForm
from .decorators import anonymous_required
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^registration/$', RegistrationAdminView.as_view(), name='registration'),
    url(r'^login/$', anonymous_required(login, redirect_to=reverse_lazy('home')),
        {'template_name': 'accounts/login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^logout/$', login_required(logout), name='logout'),
]
