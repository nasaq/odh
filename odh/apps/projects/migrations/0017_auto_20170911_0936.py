# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2017-09-11 09:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0016_historicaltask'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaltask',
            name='completed_at',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='completed_at',
            field=models.DateField(blank=True, null=True),
        ),
    ]
