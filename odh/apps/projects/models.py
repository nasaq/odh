from django.db import models
from django.utils.translation import ugettext_lazy as _
from location_field.models.plain import PlainLocationField
from django.contrib.postgres.fields import JSONField
from simple_history.models import HistoricalRecords
from forms_builder.forms.models import Form, FormEntry, FieldEntry


class Category(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    active = models.BooleanField(verbose_name=_("Active"), default=True, help_text=_(
        "Uncheck this box if you don't want this value to be available for new projects"))

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name


class ProjectType(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    active = models.BooleanField(verbose_name=_("Active"), default=True, help_text=_(
        "Uncheck this box if you don't want this value to be available for new projects"))

    class Meta:
        verbose_name = _('Project type')
        verbose_name_plural = _('Project Types')

    def __str__(self):
        return self.name


class Budget(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    active = models.BooleanField(verbose_name=_("Active"), default=True, help_text=_(
        "Uncheck this box if you don't want this value to be available for new projects"))

    class Meta:
        verbose_name = _('Budget')
        verbose_name_plural = _('Budgets')

    def __str__(self):
        return self.name


class StartIn(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    active = models.BooleanField(verbose_name=_("Active"), default=True, help_text=_(
        "Uncheck this box if you don't want this value to be available for new projects"))

    class Meta:
        verbose_name = _('Start In')
        verbose_name_plural = _('Start In')

    def __str__(self):
        return self.name


class Goals(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    active = models.BooleanField(verbose_name=_("Active"), default=True, help_text=_(
        "Uncheck this box if you don't want this value to be available for new projects"))

    class Meta:
        verbose_name = _('Goal')
        verbose_name_plural = _('Goals')

    def __str__(self):
        return self.name


class Clients(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    active = models.BooleanField(verbose_name=_("Active"), default=True, help_text=_(
        "Uncheck this box if you don't want this value to be available for new projects"))

    class Meta:
        verbose_name = _('Client')
        verbose_name_plural = _('Clients')

    def __str__(self):
        return self.name


class Blueprints(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    active = models.BooleanField(verbose_name=_("Active"), default=True, help_text=_(
        "Uncheck this box if you don't want this value to be available for new projects"))

    class Meta:
        verbose_name = _('Blueprint')
        verbose_name_plural = _('Blueprints')

    def __str__(self):
        return self.name


class Phase(models.Model):
    # Dictionary model
    name = models.CharField(verbose_name=_("Name"), max_length=200, )
    order = models.PositiveIntegerField(verbose_name=_("Order"), unique=True)

    class Meta:
        verbose_name = _('Phase')
        verbose_name_plural = _('Phases')

    def __str__(self):
        return self.name


class Projects(models.Model):
    # Projects model
    title = models.CharField(verbose_name=_("Title"), max_length=200, )
    descrition = models.CharField(verbose_name=_("Descrition"), max_length=2000, null=True)
    category = models.ForeignKey(Category, verbose_name=_("Category"), )
    type = models.ForeignKey(ProjectType, verbose_name=_("Type"), )
    components = models.CharField(verbose_name=_("Components"), max_length=2000, null=True)
    area = models.IntegerField(verbose_name=_("Area"), )
    city = models.CharField(verbose_name=_("Search by city"), max_length=255, null=True, )
    location = PlainLocationField(verbose_name=_("Location"), based_fields=['city'], zoom=7, null=True, )
    budget = models.ForeignKey(Budget, verbose_name=_("Budget"), null=True)
    start = models.ForeignKey(StartIn, verbose_name=_("Start In"), null=True)
    goal = models.ForeignKey(Goals, verbose_name=_("Goal"), null=True)
    blueprints = models.ForeignKey(Blueprints, verbose_name=_("Project blueprints exists?"), null=True)
    client = models.ForeignKey(Clients, verbose_name=_("Client"), null=True)
    phase = models.ForeignKey(Phase, verbose_name=_("Last phase"), null=True)  # last phase for project

    class Meta:
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')

    def __str__(self):
        return self.title

    def curr_tasks(self):
        ph = self.phase
        if not ph:
            ph = Phase.objects.get(order=1)
        return self.ptasks.filter(phase=ph)

    def not_complete_tasks(self):
        ph = self.phase
        if not ph:
            ph = Phase.objects.get(order=1)

        if self.ptasks.filter(phase=ph, completed=False).count() == 0:
            return False
        return True

    def next_stage_exists(self):
        if not self.phase:
            return False

        nexts = Phase.objects.filter(order__gt=self.phase.order)
        pc = ProjectCompleteStage.objects.filter(project=self, phase=self.phase)
        if nexts:
            return True
        elif not pc:
            return True
        return False


class ProjectCompleteStage(models.Model):
    # Projects Stages Complete
    completed_at = models.DateField(auto_now_add=True)
    phase = models.ForeignKey(Phase, verbose_name=_("Phase"), )
    project = models.ForeignKey(Projects, verbose_name=_("Project"), related_name="pcompleted")

    class Meta:
        verbose_name = _('Stage completed')
        verbose_name_plural = _('Stage completed')

    def __str__(self):
        return self.title


class Task(models.Model):
    # Projects Tasks
    title = models.CharField(verbose_name=_("Title"), max_length=200, )
    phase = models.ForeignKey(Phase, verbose_name=_("Phase"), null=False)
    project = models.ForeignKey(Projects, verbose_name=_("Project"), null=True, blank=False, related_name="ptasks")
    data = JSONField(verbose_name=_("Form data"), null=True, blank=True)
    completed = models.BooleanField(verbose_name=_("Completed"), default=False, )
    form = models.ForeignKey(Form, verbose_name=_("Use form"), null=True, blank=False)
    history = HistoricalRecords()
    completed_at = models.DateTimeField(null=True, blank=True)
    lastformentry = models.ForeignKey(FormEntry, verbose_name=_("Phase"), null=True, blank=True)

    def history_fields(self):
        return self.lastformentry and self.lastformentry.fields.all() or []

    class Meta:
        verbose_name = _('Task')
        verbose_name_plural = _('Tasks')

    def __str__(self):
        return self.title


class TaskEntrys(models.Model):
    # Entry for Tasks
    created_at = models.DateTimeField(null=True, blank=True)
    task = models.ForeignKey(Task, verbose_name=_("Task"), null=False, related_name="tentrys")
    entry = models.ForeignKey(FormEntry, verbose_name=_("Entry"), null=False, related_name="tfentrys")
    completed = models.BooleanField(verbose_name=_("Completed"), default=False, )

    def __str__(self):
        return self.task.title

    class Meta:
        verbose_name = _('Task update')
        verbose_name_plural = _('Task updates')
        ordering = ['-id']
