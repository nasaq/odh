from django.contrib import admin
from .models import Category, ProjectType, Budget, StartIn, Goals, Clients, Blueprints, Phase, Task, TaskEntrys
from forms_builder.forms.models import Field


class BaseOptions(admin.ModelAdmin):
    list_display = ('name', 'active')
    search_fields = ('name',)
    ordering = ('name',)


admin.site.register(Category, BaseOptions)
admin.site.register(ProjectType, BaseOptions)
admin.site.register(Budget, BaseOptions)
admin.site.register(StartIn, BaseOptions)
admin.site.register(Goals, BaseOptions)
admin.site.register(Clients, BaseOptions)
admin.site.register(Blueprints, BaseOptions)


class PhaseOptions(admin.ModelAdmin):
    list_display = ('name', 'order')
    search_fields = ('name',)
    ordering = ('order',)


admin.site.register(Phase, PhaseOptions)


class TaskEntryInline(admin.TabularInline):
    model = TaskEntrys
    fields = ('created_at', 'completed', 'values')
    readonly_fields = ('created_at', 'completed', 'values')
    can_delete = False

    def values(self, obj):
        #all values for task changes
        ret = ''
        for o in obj.entry.fields.all():
            try:
                label = Field.objects.get(id=o.field_id).label
            except:
                label = "Field was removed"
            ret += "<b>%s</b>:%s<br />" % (label, o.value)
        return ret

    values.allow_tags = True
    values.short_description = 'Values'

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class TaskOptions(admin.ModelAdmin):
    list_display = ('title', 'phase', 'project', 'completed', 'completed_at')
    fields = ('title', 'phase', 'project', 'form', 'completed', 'completed_at','lastvalues')
    search_fields = ('title',)
    ordering = ('title',)
    readonly_fields = ('completed', 'completed_at', 'data','lastvalues')
    inlines = (TaskEntryInline,)

    def lastvalues(self, obj):
        #last values for task changes
        ret = ''
        if obj.lastformentry:
            for o in obj.lastformentry.fields.all():
                try:
                    label = Field.objects.get(id=o.field_id).label
                except:
                    label = "Field was removed"
                ret += "<b>%s</b>:%s<br />" % (label, o.value)
        return ret

    lastvalues.allow_tags = True
    lastvalues.short_description = 'Last Values'

admin.site.register(Task, TaskOptions, )
