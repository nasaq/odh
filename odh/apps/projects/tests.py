from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from apps.projects.models import ProjectType, Blueprints, Budget, Goals, Clients, Category, StartIn, Projects
from apps.accounts.models import User


class ProjectsGeneralTest(TestCase):
    # test projects general
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', email='test@test.pl', password='test')
        self.client.login(username='test', password='test')
        self.ptype = ProjectType.objects.create(name='test', )
        self.blueprint = Blueprints.objects.create(name='test', )
        self.budget = Budget.objects.create(name='test', )
        self.goal = Goals.objects.create(name='test', )
        self.clients = Clients.objects.create(name='test', )
        self.category = Category.objects.create(name='test', )
        self.startin = StartIn.objects.create(name='test', )

    def test_home_url(self):
        response = self.client.get(reverse('home'), {}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_add_url(self):
        response = self.client.post(reverse('project-add'),
                                    {'title': 'test', 'area': 1, 'components': 'test', 'descrition': 'test',
                                     'category': self.category.id,
                                     'type': self.ptype.id, 'budget': self.budget.id, 'start': self.startin.id,
                                     'goal': self.goal.id, 'blueprints': self.blueprint.id, 'client': self.clients.id},
                                    follow=True)

        self.assertEqual(response.status_code, 200)


class ProjectsDetailedTest(TestCase):
    # test projects detailed functions
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', email='test@test.pl', password='test')
        self.client.login(username='test', password='test')
        self.ptype = ProjectType.objects.create(name='test', )
        self.blueprint = Blueprints.objects.create(name='test', )
        self.budget = Budget.objects.create(name='test', )
        self.goal = Goals.objects.create(name='test', )
        self.clients = Clients.objects.create(name='test', )
        self.category = Category.objects.create(name='test', )
        self.startin = StartIn.objects.create(name='test', )
        self.project = Projects.objects.create(title='test', area=1, components='test',
                                               category=self.category, type=self.ptype, budget=self.budget,
                                               start=self.startin, goal=self.goal, blueprints=self.blueprint,
                                               client=self.clients, )

    def test_view_url(self):
        # test detail view url
        response = self.client.get(reverse('project-view', kwargs={'pk': self.project.id, }), {}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_edit_get_url(self):
        # test edit GET view url
        response = self.client.get(reverse('project-edit', kwargs={'pk': self.project.id, }), {}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_edit_post_post(self):
        # test edit POST view url
        response = self.client.get(reverse('project-edit', kwargs={'pk': self.project.id, }),
                                   {'title': 'test', 'area': 1, 'components': 'test', 'descrition': 'test',
                                    'category': self.category.id,
                                    'type': self.ptype.id, 'budget': self.budget.id, 'start': self.startin.id,
                                    'goal': self.goal.id, 'blueprints': self.blueprint.id, 'client': self.clients.id},
                                   follow=True)
        self.assertEqual(response.status_code, 200)

    def test_delete_get_url(self):
        # test delete GET view url
        response = self.client.get(reverse('project-delete', kwargs={'pk': self.project.id, }), {}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_delete_post_url(self):
        # test delete POST view url
        response = self.client.post(reverse('project-delete', kwargs={'pk': self.project.id, }), {}, follow=True)
        self.assertEqual(response.status_code, 200)
