from django.conf.urls import url

from .views import HomeView, ProjectAddView, ProjectEditView, ProjectDeleteView, ProjectDetailView, TaskView, \
    ProjectConfirmationView

urlpatterns = [
    url(r'^home/$', HomeView.as_view(), name='home'),
    url(r'^add/$', ProjectAddView.as_view(), name='project-add'),
    url(r'^edit/(?P<pk>\d+)/$', ProjectEditView.as_view(), name='project-edit'),
    url(r'^delete/(?P<pk>\d+)/$', ProjectDeleteView.as_view(), name='project-delete'),
    url(r'^details/(?P<pk>\d+)/$', ProjectDetailView.as_view(), name='project-view'),
    url(r'^task/(?P<pk>\d+)/$', TaskView.as_view(), name='task-view'),
    url(r'^confirm/(?P<pk>\d+)/$', ProjectConfirmationView.as_view(), name='project-confirm'),
]
