from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import ProjectForm, TaskForm
from django.core.urlresolvers import reverse_lazy
from .models import Projects, Phase, Task, ProjectCompleteStage, TaskEntrys
from forms_builder.forms.models import FormEntry, FieldEntry
import datetime
from django.http import Http404, HttpResponseRedirect


class HomeView(LoginRequiredMixin, TemplateView):
    # home, project list view
    template_name = 'projects/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['projects'] = Projects.objects.all().order_by('-id')
        return context


class ProjectAddView(LoginRequiredMixin, CreateView):
    # project add form
    form_class = ProjectForm
    template_name = 'projects/project-manage.html'
    success_url = reverse_lazy('home', kwargs={})


class ProjectEditView(LoginRequiredMixin, UpdateView):
    # project edit form
    form_class = ProjectForm
    template_name = 'projects/project-manage.html'
    model = Projects

    def get_success_url(self):
        return reverse_lazy('project-view', kwargs={'pk': self.object.id, })


class ProjectDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'projects/project-delete.html'
    success_url = reverse_lazy('home', kwargs={})
    model = Projects


class ProjectDetailView(LoginRequiredMixin, DetailView):
    # project add view
    template_name = 'projects/project-view.html'
    success_url = reverse_lazy('home', kwargs={})
    model = Projects

    def get_context_data(self, **kwargs):
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        context['phases'] = Phase.objects.all().order_by('order')
        return context


class TaskView(LoginRequiredMixin, UpdateView):
    # project edit form
    form_class = TaskForm
    template_name = 'projects/task-manage.html'
    model = Task

    def get_context_data(self, **kwargs):
        context = super(TaskView, self).get_context_data(**kwargs)
        context['obid'] = self.object.project.id
        return context

    def form_valid(self, form):
        response = super(TaskView, self).form_valid(form)
        dt = datetime.datetime.now()
        # create form entry instance for task
        fe = FormEntry(form=self.object.form, entry_time=dt)
        fe.save()
        allrequired = True
        for o in self.object.form.fields.all():
            # save value for each form field
            fielde = FieldEntry(entry=fe, field_id=o.id, value=self.request.POST.get(o.slug, None))
            fielde.save()
            # check if any required fiels is empty
            if o.required and not self.request.POST.get(o.slug, None) and allrequired:
                allrequired = False

        self.object.lastformentry = fe

        # change task as completed
        self.object.completed = allrequired
        self.object.completed_at = allrequired and dt or None
        self.object.save()
        # save task fields values
        te = TaskEntrys(created_at=dt, task=self.object, entry=fe, completed=allrequired)
        te.save()

        return response

    def get_success_url(self):
        return reverse_lazy('project-view', kwargs={'pk': self.object.project.id, })


class ProjectConfirmationView(ProjectDetailView):
    def post(self, request, **kwargs):
        self.object = self.get_object()
        if self.object.not_complete_tasks():
            raise Http404()

        context = super(ProjectConfirmationView, self).get_context_data(**kwargs)
        # get current phase
        ph = self.object.phase
        if not ph:
            ph = Phase.objects.get(order=1)

        # set info on about stage confirmation
        c = ProjectCompleteStage(phase=ph, project=self.object)
        c.save()
        # get next phase
        nexts = Phase.objects.filter(order__gt=ph.order).order_by('order')
        if nexts:
            ph = nexts[0]
            self.object.phase = ph
            self.object.save()
        return HttpResponseRedirect(reverse_lazy('project-view', kwargs={'pk': self.object.id, }))
