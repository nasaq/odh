from django import forms
from .models import Projects
from django.utils.translation import ugettext_lazy as _
from .models import Category, ProjectType, Budget, StartIn, Goals, Blueprints, Clients, Task
from django.db.models.query_utils import Q


class ProjectForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)

        super(ProjectForm, self).__init__(*args, **kwargs)
        # add placholders
        self.fields['title'].widget.attrs['placeholder'] = _('Title')
        self.fields['descrition'].widget.attrs['placeholder'] = _('Descrition')
        self.fields['components'].widget.attrs['placeholder'] = _('Components')
        self.fields['area'].widget.attrs['placeholder'] = _('Area')
        self.fields['location'].widget.attrs['placeholder'] = _('Location')
        self.fields['city'].widget.attrs['placeholder'] = _('Search by City')

        # set active dictionaries for new and edited project
        categoryqs = Category.objects.filter(active=True).order_by('name')
        typeqs = ProjectType.objects.filter(active=True).order_by('name')
        budgetqs = Budget.objects.filter(active=True).order_by('name')
        startinqs = StartIn.objects.filter(active=True).order_by('name')
        goalsqs = Goals.objects.filter(active=True).order_by('name')
        blueprintsqs = Blueprints.objects.filter(active=True).order_by('name')
        clientssqs = Clients.objects.filter(active=True).order_by('name')

        if instance and instance.category:
            categoryqs = Category.objects.filter(Q(active=True) | Q(id=self.instance.category.id)).order_by('name')

        if instance and instance.type:
            categoryqs = ProjectType.objects.filter(Q(active=True) | Q(id=self.instance.type.id)).order_by('name')

        if instance and instance.budget:
            categoryqs = Budget.objects.filter(Q(active=True) | Q(id=self.instance.budget.id)).order_by('name')

        if instance and instance.start:
            categoryqs = StartIn.objects.filter(Q(active=True) | Q(id=self.instance.start.id)).order_by('name')

        if instance and instance.goal:
            categoryqs = Goals.objects.filter(Q(active=True) | Q(id=self.instance.goal.id)).order_by('name')

        if instance and instance.blueprints:
            categoryqs = Blueprints.objects.filter(Q(active=True) | Q(id=self.instance.blueprints.id)).order_by('name')

        if instance and instance.client:
            categoryqs = Clients.objects.filter(Q(active=True) | Q(id=self.instance.client.id)).order_by('name')

        self.fields['category'].widget.choices = [('', _("Select Category"))] + [(x.id, x.name) for x in categoryqs]
        self.fields['type'].widget.choices = [('', _("Select Type"))] + [(x.id, x.name) for x in typeqs]
        self.fields['budget'].widget.choices = [('', _("Select Budget"))] + [(x.id, x.name) for x in budgetqs]
        self.fields['start'].widget.choices = [('', _("Select Start in"))] + [(x.id, x.name) for x in startinqs]
        self.fields['goal'].widget.choices = [('', _("Select Goal"))] + [(x.id, x.name) for x in goalsqs]
        self.fields['blueprints'].widget.choices = [('', _("Select Blueprint"))] + [(x.id, x.name) for x in
                                                                                    blueprintsqs]
        self.fields['client'].widget.choices = [('', _("Select Client"))] + [(x.id, x.name) for x in clientssqs]

    class Meta:
        model = Projects
        widgets = {
            'descrition': forms.Textarea(attrs={'rows': 4}),
        }
        fields = (
            'title', 'descrition', 'category', 'type', 'components', 'area', 'location', 'city', 'budget', 'start',
            'goal',
            'blueprints', 'client',)


class TaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)

        super(TaskForm, self).__init__(*args, **kwargs)
        # add placholders
        # self.fields['title'].widget.attrs['placeholder'] = _('Title')

    class Meta:
        model = Task
        fields = ('data',)
