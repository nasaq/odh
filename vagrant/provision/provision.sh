#!/usr/bin/env bash

echo "Installing system packages"
apt-get update -y
apt-get install -y build-essential python-pip python-dev mc git libffi-dev dos2unix

# Dependencies for virtualenv
apt-get install -y libjpeg8 libjpeg8-dev libtiff-dev zlib1g-dev libfreetype6-dev liblcms2-dev libxml2-dev libxslt1-dev libffi-dev libssl-dev swig libyaml-dev libpython2.7-dev

# graphics support for Pillow (jpg, png etc)
apt-get -y build-dep python-imaging
apt-get install -y libjpeg8 libjpeg62-dev libfreetype6 libfreetype6-dev

dos2unix /vagrant/odh/rs.sh

# PostgreSQL
apt-get install -y libpq-dev postgresql postgresql-server-dev-all
sudo -u postgres createuser -d -s vagrant
sudo -u postgres createdb -O vagrant vagrant
sudo -u postgres psql -U postgres -d postgres -c "ALTER USER vagrant WITH PASSWORD 'vagrant';"

echo "Setting up pip and virtualenv"
pip install virtualenv
pip install virtualenvwrapper

echo "Creating virtualenv and configuring app"
su - ubuntu -c "mkdir -p /home/ubuntu/envs"
su - ubuntu -c "export WORKON_HOME=~/envs && \
source /usr/local/bin/virtualenvwrapper.sh && \
mkvirtualenv -p /usr/bin/python3 vagrant && \
pip install -r /vagrant/odh/requirements/local.txt && \
cd /vagrant/odh && \
python manage.py migrate"

cp -i /vagrant/vagrant/.env.example /vagrant/.env
rm /home/ubuntu/.profile
\cp -i /vagrant/vagrant/templates/.profile /home/ubuntu/.profile

dos2unix /home/ubuntu/.bashrc