#!/usr/bin/env bash
echo "Provision always:"
sudo apt-get install -y memcached

chmod +x /vagrant/odh/rs.sh

su - ubuntu -c "
export WORKON_HOME=~/envs && \
source /usr/local/bin/virtualenvwrapper.sh && \
pip install --upgrade pip  && \
workon vagrant && \
pip install -r /vagrant/odh/requirements/local.txt"
