ODH
===========================
Installation
-------------------------

1. Download code.

2. Run virtual machine:
`vagrant up` or `vagrant up --provider virtualbox`

3. Connect to vm:
`vagrant ssh`

4. Create postgres tables:
`python manage.py migrate`

5. Create superuser:
`python manage.py createsuperuser`

6. Run server:
`rs`

7. Web browser:
- http://localhost:8888/